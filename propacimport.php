<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
include_once 'classes/ImportHelper.php';
class Propacimport extends Module
{
    protected $config_form = false;
    public $import_table_customer = '';
    public $import_table_giacenze = '';
    public $import_table_conai = '';
    protected $importHelper = null;
    protected $csv_dir = '';
    protected $html = '';
    protected $operation_logs = '';

    public function __construct()
    {
        $this->name = 'propacimport';
        $this->tab = 'migration_tools';
        $this->version = '1.0.0';
        $this->author = 'OrangePix Srl';
        $this->need_instance = 1;
        $this->import_table_customer = _DB_PREFIX_.'propacimport_customer';
        $this->import_table_giacenze = _DB_PREFIX_.'propacimport_giacenze';
        $this->import_table_conai = _DB_PREFIX_.'conai_values';
        $this->importHelper = new ImportHelper();
        $this->csv_dir = dirname(__FILE__).'/csv/';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Propac Data Import | OrangePix Srl');
        $this->description = $this->l('Import Data from CSV');

        $this->confirmUninstall = $this->l('This action deactivates the classes that this module has modified.');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');
        Tools::clearAllCache();
        $this->createAttributeGroupPallet();

        return parent::install() &&
          $this->registerHook('header') &&
          $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        include(dirname(__FILE__).'/sql/uninstall.php');

        //Delete override Files
        $this->DeleteOverrideFiles();
        Tools::clearAllCache();
        parent::uninstall();

        return true;
    }

    //Scan files in ThisModule/override/ Folder
    public function ScanOverrideFiles($directory){
        $files = array();
        $dir = array_diff(scandir($directory), array('..', '.'));
        foreach ($dir as $folder){
            if (is_dir($directory.$folder)){
                $files[] = $this->ScanOverrideFiles($directory.$folder.'/');
            }else{
                $files[] = $directory.$folder;
            }
        }
        return $files;
    }

    //Delete files used by this module in rootProyect/override/ Folder
    public function DeleteOverrideFiles(){
        //Register Override Files for Uninstall
        $directory = $this->local_path.'override/';
        $overrides = json_encode($this->ScanOverrideFiles($directory)) ;
        $overrides = str_replace('[','',$overrides);
        $overrides = str_replace(']','',$overrides);
        $overrides = explode(',',stripslashes($overrides));


        foreach ($overrides as $file){
            $file =  explode('override/',trim($file,"\"") )[1];
            if (file_exists(_PS_OVERRIDE_DIR_.$file) && !is_dir(_PS_OVERRIDE_DIR_.$file)){
                try {
                    unlink(_PS_OVERRIDE_DIR_.$file);
                }catch (Exception $e){
                    $this->displayError($e->getMessage());
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Load the configuration form
     *
     * @throws PrestaShopException
     */
    public function getContent()
    {
        $this->html = '';
        $this->operation_logs = '';
        /**
         * If values have been submitted in the form, process.
         */

        if (((bool)Tools::isSubmit('import')) == true) {
            if (((bool)Tools::isSubmit('file_csv_customer')) == true)
                $this->importClients();

            if (((bool)Tools::isSubmit('file_csv_giacenze')) == true)
                $this->importGiacenze();

            if (((bool)Tools::isSubmit('file_csv_conai')) == true)
                $this->importConai();

            if (((bool)Tools::isSubmit('file_csv_iva')) == true)
                $this->importIVA();
        }

        $this->context->smarty->assign('module_dir', $this->_path);
        $output = $this->html;
        $output .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
        $output .= $this->renderFormClient();
        $output .= $this->renderFormGiacenze();
        $output .= $this->renderFormConai();
        $output .= $this->renderFormIVA();
        if (!empty($this->operation_logs)){
            $output .= '<div class="logs"><h4>'.$this->l('Updated Records').'</h4>';
            $output .= $this->operation_logs;
            $output .= '</div>';
        }
        return $output;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderFormClient()
    {
        $fields_form_csv = array(
          'form' => array(
            'legend' => array(
              'title' => $this->l('Import Customers'),
              'icon' => 'icon-cogs',
            ),
            'input' => array(
              array(
                'type' => 'file',
                'label' => $this->l('Import Clients (file csv)'),
                'name' => 'file_csv_customer',
              ),
            ),
            'submit' => array(
              'title' => $this->l('Load'),
              'icon' => 'process-icon-download-alt',
              'name' => 'submitClientCsv',
            ),
          ),
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
          ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->identifier = $this->identifier;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&import=1';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
          'languages' => $this->context->controller->getLanguages(),
          'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form_csv));
    }

    /**
     * @return string
     * @throws PrestaShopException
     */
    protected function renderFormGiacenze()
    {
        $fields_form_csv = array(
          'form' => array(
            'legend' => array(
              'title' => $this->l('Import Giacenze'),
              'icon' => 'icon-cogs',
            ),
            'input' => array(
              array(
                'type' => 'file',
                'label' => $this->l('Import Giacenze (file csv)'),
                'name' => 'file_csv_giacenze',
              ),
            ),
            'submit' => array(
              'title' => $this->l('Load'),
              'icon' => 'process-icon-download-alt',
              'name' => 'submitGiacenzaCsv',
            ),
          ),
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
          ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->identifier = $this->identifier;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&import=1';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
          'languages' => $this->context->controller->getLanguages(),
          'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form_csv));
    }

    /**
     * @return string
     * @throws PrestaShopException
     */
    protected function renderFormConai()
    {
        $fields_form_csv = array(
          'form' => array(
            'legend' => array(
              'title' => $this->l('Import Conai'),
              'icon' => 'icon-cogs',
            ),
            'input' => array(
              array(
                'type' => 'file',
                'label' => $this->l('Import Conai (file csv)'),
                'name' => 'file_csv_conai',
              ),
            ),
            'submit' => array(
              'title' => $this->l('Load'),
              'icon' => 'process-icon-download-alt',
              'name' => 'submitConaiCsv',
            ),
          ),
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
          ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->identifier = $this->identifier;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&import=1';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
          'languages' => $this->context->controller->getLanguages(),
          'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form_csv));
    }

    protected function renderFormIVA()
    {
        $fields_form_csv = array(
          'form' => array(
            'legend' => array(
              'title' => $this->l('Import IVA dei Prodotti'),
              'icon' => 'icon-cogs',
            ),
            'input' => array(
              array(
                'type' => 'file',
                'label' => $this->l('Import IVA (file csv)'),
                'name' => 'file_csv_iva',
              ),
            ),
            'submit' => array(
              'title' => $this->l('Load'),
              'icon' => 'process-icon-download-alt',
              'name' => 'submitIvaCsv',
            ),
          ),
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
          ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->identifier = $this->identifier;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&import=1';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
          'languages' => $this->context->controller->getLanguages(),
          'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form_csv));
    }


    /**
     * Import Customers csv
     */
    public function importClients($filePath = null){
        $file = Tools::fileAttachment('file_csv_customer');
        if ($file != null || $file['mime'] === 'text/csv'){
            if (!file_exists($this->csv_dir)){
                mkdir($this->csv_dir,0777,true);
            }
            move_uploaded_file($file['tmp_name'], $this->csv_dir.$file['name']);
            try {
                $this->importHelper->importToMySQL($this->csv_dir.$file['name'], $this->import_table_customer);
                $this->createIvaGroups($this->import_table_customer);
                $this->updateCustomerCodGest();
            } catch (PrestaShopDatabaseException $e) {
                $this->html .= $this->displayError($e->getMessage());
            }
            $this->html .= $this->displayConfirmation($this->l('Dati aggiornati con successo'));
        }elseif ($filePath != null){
            try {
                $this->importHelper->importToMySQL($filePath, $this->import_table_customer);
                $this->createIvaGroups($this->import_table_customer);
                $this->updateCustomerCodGest();
            } catch (PrestaShopDatabaseException $e) {
                return json_encode(array('response' => $e->getMessage()));
            }
            return json_encode(array('response' => 'success')) ;
        }
        else
            $this->html .= $this->displayWarning($this->l('Carica un file valido'));
    }

    /**
     * Update Codice Gestionale nella tabella customer
     */
    public function updateCustomerCodGest(){
        try {
            $customersToUpdateList = @DB::getInstance()->executeS('
            SELECT
                c.id_customer ,
                poc.codice_cliente ,
                poc.aliquota_iva
            FROM
                ' . $this->import_table_customer . ' poc ,
                ' . _DB_PREFIX_ . 'customer c
            LEFT JOIN ' . _DB_PREFIX_ . 'address a ON c.id_customer = a.id_customer
            WHERE
                poc.dni <> "" 
                AND 
                poc.dni IS NOT NULL
            AND(
                poc.dni = a.vat_number
                OR poc.dni = a.dni
            )
            GROUP BY
                c.id_customer
            ');

        } catch (PrestaShopDatabaseException $e) { return $e->getMessage();}
        $this->operation_logs .= '<ul>';
        foreach ($customersToUpdateList as $singleCustomerToUpdate) {

            $idVatGroups = $this->getIdVatGroup($singleCustomerToUpdate['aliquota_iva']);
            $customer = new Customer($singleCustomerToUpdate['id_customer']);
            $customer->setCodGest($singleCustomerToUpdate['codice_cliente']);
            if (!empty($idVatGroups)){
                $customer->addGroups($idVatGroups);
                $customer->id_default_group = $idVatGroups['id_group'];
            }
            $customer->update();
            $this->updateCustomerOrderCodGest($customer->id, $customer->cod_gest);
            $this->operation_logs .= '<li>'.$this->l('Customer') .': '.$singleCustomerToUpdate['id_customer'].' - ( '.$this->l('Code').' :'.$singleCustomerToUpdate['codice_cliente'].' )</li>';
        }
        $this->operation_logs .= '</ul>';

    }

    /*
     * Upate all the cod_gest in the customer orders
     */
    private function updateCustomerOrderCodGest($idCustomer, $customerCodGest)
    {
        try {
            $ordersToUpdateList = @DB::getInstance()->executeS('
            SELECT
                o.id_order
            FROM
                '. _DB_PREFIX_ .'orders o
            WHERE
                o.id_customer = '. $idCustomer .'
            ');

        } catch (PrestaShopDatabaseException $e) { return $e->getMessage();}
        $this->operation_logs .= '<ul>';
        foreach ($ordersToUpdateList as $singleOrderToUpdate) {
            $order = new Order($singleOrderToUpdate['id_order']);
            $order->customer_cod_gest = $customerCodGest;
            $order->update();
            $this->operation_logs .= '<li>'.$this->l('Order') .': '.$singleOrderToUpdate['id_order'].' - ( '.$this->l('Code').' :'.$customerCodGest.' )</li>';
        }
        $this->operation_logs .= '</ul>';
    }

    /*
     * Return the id of the group with the vat value = $aliquota_iva
     */
    private function getIdVatGroup($aliquota_iva) {
        return current(
          DB::getInstance()->executeS('
                SELECT
                    g.id_group
                FROM
                    '. _DB_PREFIX_ .'group g
                WHERE
                    g.aliquota_iva = '. $aliquota_iva .'
                    AND g.id_group <> 1
                GROUP BY
                    g.aliquota_iva
            ')
        );
    }

    /**
     * @param string $table tableName with prefix
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function createIvaGroups($table){
        $vatsList = DB::getInstance()->executeS('
        SELECT
            `aliquota_iva`
        FROM
            ' . $table . '
        WHERE
            `aliquota_iva` > 0
        GROUP BY
            aliquota_iva
        ');
        foreach ($vatsList as $singleIva){
            if (!Group::searchByName('GRUPPO IVA '.$singleIva['aliquota_iva'])){
                $group = new Group();
                foreach (Language::getLanguages(false) as $lang){
                    $group->name[$lang['id_lang']] = 'GRUPPO IVA '.$singleIva['aliquota_iva'];
                }
                $group->show_prices = 1;
                $group->reduction = 0;
                $group->price_display_method = Group::getDefaultPriceDisplayMethod();
                $group->aliquota_iva = (int)$singleIva['aliquota_iva'];
                $group->add();
            }
        }
    }

    /**
     * Import Giacenze csv
     */
    public function importGiacenze($filePath = null){
        $file = Tools::fileAttachment('file_csv_giacenze');
        if ($file != null || $file['mime'] === 'text/csv'){
            if (!file_exists($this->csv_dir)){
                mkdir($this->csv_dir,0777,true);
            }
            move_uploaded_file($file['tmp_name'], $this->csv_dir.$file['name']);
            try {
                $this->importHelper->importToMySQL($this->csv_dir.$file['name'], $this->import_table_giacenze,false,0,',');
                $this->getGiacenzeFromDB();
            } catch (PrestaShopDatabaseException $e) {
                $this->html .= $this->displayError($e->getMessage());
            }
            $this->html .= $this->displayConfirmation($this->l('Dati aggiornati con successo'));
        }elseif ($filePath != null){
            try {
                $this->importHelper->importToMySQL($filePath, $this->import_table_giacenze,false,0,',');
                $this->getGiacenzeFromDB();
            } catch (PrestaShopDatabaseException $e) {
                return json_encode(array('response' => $e->getMessage()));
            }
            return json_encode(array('response' => 'success'));
        }
        else
            $this->html .= $this->displayWarning($this->l('Carica un file valido'));
    }

    /**
     * Update quantity nella tabella product_attribute
     * @return string
     */
    public function getGiacenzeFromDB(){
        try {
            $listCombinationQuantity = @DB::getInstance()->executeS('
                SELECT
                    sa.id_stock_available,
                    a.id_product_attribute,
                    a.id_product,
                    g.disponibilita,
                    g.unita_vendita,
                    g.pallet
                FROM
                    ' . _DB_PREFIX_ . 'propacimport_giacenze g
                LEFT JOIN ' . _DB_PREFIX_ . 'product_attribute a ON g.codice_prodotto = a.reference
                left join ' . _DB_PREFIX_ . 'stock_available sa on sa.id_product_attribute = a.id_product_attribute
                WHERE
                    a.id_product_attribute IS NOT NULL
            ');
            $listProductsWithNoCombinationQuantity = @DB::getInstance()->executeS('
                SELECT
                    sa.id_stock_available ,
                    p.id_product,
                    g.disponibilita,
                    g.unita_vendita,
                    g.pallet
                FROM
                    ' . _DB_PREFIX_ . 'propacimport_giacenze g
                LEFT JOIN ' . _DB_PREFIX_ . 'product p ON g.codice_prodotto = p.reference
                LEFT JOIN ' . _DB_PREFIX_ . 'stock_available sa ON sa.id_product = p.id_product
                WHERE
                    p.id_product IS NOT NULL
            ');

        } catch (PrestaShopDatabaseException $e) { return $e->getMessage();}
        $this->updateStocks($listCombinationQuantity);
        $this->updateStocks($listProductsWithNoCombinationQuantity);

    }

    public function updateStocks($stocksList) {
        $this->operation_logs .= '<ul>';
        foreach ($stocksList as $singleStock){
            if($singleStock['id_stock_available']) { //if stock available isn't been created or it's been deleted
                $stockAvailable = new StockAvailable($singleStock['id_stock_available']);
            } else {
                $stockAvailable = new StockAvailable();
                $stockAvailable->id_product = $singleStock['id_product'];
                $stockAvailable->id_product_attribute = ($singleStock['id_product_attribute']) ? $singleStock['id_product_attribute'] : 0;
                $stockAvailable->depends_on_stock = StockAvailable::dependsOnStock($singleStock['id_product']);
                $stockAvailable->out_of_stock = StockAvailable::outOfStock($singleStock['id_product']);
            }

            if ((bool)$singleStock['id_product_attribute']){
                $combination = new Combination($singleStock['id_product_attribute']);
                $this->updatePallet($combination,$singleStock['pallet']);
                $combination->quantity_per_pack = ((int)$singleStock['unita_vendita'] > 0) ? (int)$singleStock['unita_vendita'] : 1;
                $combination->save();
            }
            $stockAvailable->quantity = $this->getQuantityByCode($singleStock['disponibilita']);
            ($stockAvailable->id) ? $stockAvailable->update() : $stockAvailable->add();
            $this->operation_logs .= '<li>'.$this->l('StockAvailable/Combination/Product') .' : '.$singleStock['id_stock_available'].'/'.$singleStock['id_product_attribute'].'/'.$singleStock['id_product'].' - ( '.$this->l('Codice giacenza') .' : '.$singleStock['disponibilita'].' )</li>';
        }
        $this->operation_logs .= '</ul>';
    }

    /**
     * @param $combination Combination
     * @param $quantity_pallet
     * @return bool|void
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function updatePallet(&$combination, $quantity_pallet,$sconto_pallet = 0){
        $this->createAttributeGroupPallet();
        $attributes = array_column($combination->getAttributesName($this->context->language->id),'id_attribute');
        $id_attribute_group = (int)Configuration::get('ID_ATTRIBUTE_GROUP_PALLET');
        if (Attribute::isAttribute($id_attribute_group,strval($quantity_pallet),$this->context->language->id)){
            foreach (AttributeGroup::getAttributes($this->context->language->id,$id_attribute_group) as $attribute){
                if ($id = array_search($attribute['id_attribute'],$attributes)){
                    unset($attributes[$id]);
                }
                if ($attribute['name'] == strval($quantity_pallet)){
                    $attributes[] = $attribute['id_attribute'];
                    $combination->pallet = (int)$quantity_pallet;
                    $combination->sconto_pallet = (float)$sconto_pallet;
                    return $combination->setAttributes($attributes);
                }
            }
        }
        if ($quantity_pallet == 0)
            return;

        $attribute = new Attribute();
        foreach (Language::getIDs() as $id_lang){
            $attribute->name[$id_lang] = $quantity_pallet;
        }
        $attribute->id_attribute_group = $id_attribute_group;
        $attribute->default = $quantity_pallet;
        $attribute->add();
        $attributes[] = $attribute->id;
        $combination->pallet = (int)$quantity_pallet;
        $combination->sconto_pallet = (float)$sconto_pallet;
        $combination->setAttributes($attributes);
    }

    /** Exec during install
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function createAttributeGroupPallet(){
        $list_groups = AttributeGroup::getAttributesGroups($this->context->language->id);
        $names = array_column($list_groups,'name');
        if ($exist = array_search('Pallet',$names,true)){
            Configuration::updateValue('ID_ATTRIBUTE_GROUP_PALLET',$list_groups[$exist]['id_attribute_group']);
            return true;
        }
        $attribute_group = new AttributeGroup();
        foreach (Language::getIDs() as $id_lang){
            $attribute_group->name[$id_lang] = 'Pallet';
            $attribute_group->public_name[$id_lang] = 'Pallet';
        }
        $attribute_group->group_type = 'select';
        $attribute_group->add();
        Configuration::updateValue('ID_ATTRIBUTE_GROUP_PALLET',$attribute_group->id);
    }

    public function getQuantityByCode($code) {
        switch ($code) {
            case 1:
                return 9999;
                break;
            case 2:
                return 20000;
                break;
            default:
                return 0;
                break;
        }
    }


    /**
     *  Import Conai csv
     */
    public function importConai($filePath = null){
        $file = Tools::fileAttachment('file_csv_conai');
        if ($file != null || $file['mime'] === 'text/csv'){
            if (!file_exists($this->csv_dir)){
                mkdir($this->csv_dir,0777,true);
            }
            move_uploaded_file($file['tmp_name'], $this->csv_dir.$file['name']);
            try {
                $this->importHelper->importToMySQL($this->csv_dir.$file['name'], $this->import_table_conai,false,0,'|');
                $this->updateConai();
            } catch (PrestaShopDatabaseException $e) {
                $this->html .= $this->displayError($e->getMessage());
            }
            $this->html .= $this->displayConfirmation($this->l('Dati aggiornati con successo'));
        }elseif ($filePath != null){
            try {
                $this->importHelper->importToMySQL($filePath, $this->import_table_conai,false,0,'|');
                $this->updateConai();
            } catch (PrestaShopDatabaseException $e) {
                return json_encode(array('response' => $e->getMessage()));
            }
            return json_encode(array('response' => 'success'));
        }
        else
            $this->html .= $this->displayWarning($this->l('Carica un file valido'));
    }

    public function updateConai(){
        try {
            $listConai = @DB::getInstance()->executeS('
                SELECT
                    pa.id_product_attribute ,
                    cv.importo_conai
                FROM
                    ' . _DB_PREFIX_ . 'product_attribute pa
                INNER JOIN ' . _DB_PREFIX_ . 'conai_values cv ON(
                    pa.reference = cv.codice_prodotto
                    AND cv.codice_prodotto <> ""
                    AND cv.codice_prodotto IS NOT NULL
                )
            ');

        } catch (PrestaShopDatabaseException $e) { return $e->getMessage();}
        $this->operation_logs .= '<ul>';
        foreach ($listConai as $singleConai){
            $combination = new Combination($singleConai['id_product_attribute']);
            $combination->conai = $singleConai['importo_conai'];
            $combination->update();
            $this->operation_logs .= '<li>'.$this->l('Product/Combination') .' : '.$combination->id_product .'/'. $combination->id .' - ( '.$this->l('Conai') .' : '.$singleConai['importo_conai'].' )</li>';
        }
        $this->operation_logs .= '</ul>';

    }

    public function importIVA($filePath = null){
        $file = Tools::fileAttachment('file_csv_iva');
        if ($file != null || $file['mime'] === 'text/csv'){
            if (!file_exists($this->csv_dir)){
                mkdir($this->csv_dir,0777,true);
            }
            move_uploaded_file($file['tmp_name'], $this->csv_dir.$file['name']);
            try {
                $products = $this->readCSV($this->csv_dir.$file['name'],['reference','iva'],0,';');
                $this->updateIVA($products);
            } catch (PrestaShopDatabaseException $e) {
                $this->html .= $this->displayError($e->getMessage());
            }
            $this->html .= $this->displayConfirmation($this->l('Dati IVA aggiornati con successo'));
        }elseif ($filePath != null){
            try {
                $products = $this->readCSV($filePath,['reference','iva'],0,';' );
                $this->updateIVA($products);
            } catch (PrestaShopDatabaseException $e) {
                return json_encode(array('response' => $e->getMessage()));
            }
            return json_encode(array('response' => 'success'));
        }
        else
            $this->html .= $this->displayWarning($this->l('Carica un file valido'));
    }

    public function updateIVA($array_iva){
        $tax_groups = TaxRulesGroup::getAssociatedTaxRatesByIdCountry($this->context->country->id);
        foreach ($array_iva as $row) {
            $id_tax_group = array_search(str_replace('%','',$row['iva']),$tax_groups);
            if ($id_tax_group){
                $productObject = new Product(Product::getIdByReference($row['reference']));
                if($productObject->id){
                    $productObject->id_tax_rules_group = $id_tax_group;
                    $productObject->save();
                }

            }
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    /**
     * @param string $file path to file
     * @param array $header array columns
     * @param int $skip skip the first N rows
     *
     * @param string $separator Comma
     * @return array
     */
    protected function readCSV($file, $header = ['reference','iva'],$skip = 0, $separator=';')
    {
        $handle = $this->importHelper->openCsvFile($file, $skip);
        $products = array();
        for ($current_line = 0; $line = fgetcsv($handle, MAX_LINE_SIZE, $separator); $current_line++) {

            if (empty($line)) {
                continue;
            }

            // fetch data
            try {
                $i = 0;
                foreach ($header as $head) {
                    $products[$current_line][$head] = @$line[$i];
                    $i++;
                }
            }catch (Exception $e){
                PrestaShopLogger::addLog($e->getMessage() . " - (" . implode(', ', $line) . ")", 2);
            }
        }
        $this->importHelper->closeCsvFile($handle);

        return $products;
    }
}
