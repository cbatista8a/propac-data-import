<?php
include_once "../classes/FileUploader.php";
include_once "../../../config/defines.inc.php";
include_once "../../../classes/Tools.php";


$fileUploader = new FileUploader(50*1024*1024, 'file');

$destination = _PS_ROOT_DIR_ . "/modules/propacimport/csv/";

if (array_key_exists("type", $_GET)){
    $response['message'] = $fileUploader->upload($_FILES, $destination, $_GET["type"]);
    $response['error'] = ($response['message'] == 'Success') ? false : true;
}

$response = ($response) ? $response : ['error' => false, 'message' => 'Type not set'];

header('Content-Type: application/json');
echo json_encode($response);



