<?php

@ini_set('max_execution_time', 0);

/** No max line limit since the lines can be more than 4096. Performance impact is not significant. */
define('MAX_LINE_SIZE', 0);

class ImportHelper
{
    public $SEPARATOR = ',';
    protected $db = null;

    public function __construct()
    {
        $this->db = Db::getInstance(_PS_USE_SQL_SLAVE_);
    }

    /**
     * @param string $file  absolute path to .csv file
     * @param string $table table_name with prefix
     * @param bool   $contain_id_column false if don't exist in csv
     *
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    public function importToMySQL($file, $table, $contain_id_column = true, $skip = 0, $separator = ',')
    {
        $this->SEPARATOR = $separator;
        $headers = $this->db->executeS('SELECT * FROM information_schema.columns WHERE table_name = "'.$table.'" ;'); //get columns name of table from database
        $headers = array_column($headers,'COLUMN_NAME');
        if (!$contain_id_column)
            unset($headers[0]);
        return $this->insertCSV($file, $headers, $table, $skip);
    }


    // Internal Methods ================================================================================================

    /**
     * @param string $file   path to file
     * @param array  $header array columns
     * @param string $table  table_name width prefix
     * @param int    $skip   skip the first N rows
     *
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    protected function insertCSV($file, $header, $table,$skip = 0)
    {
        $this->db->execute('TRUNCATE TABLE `' .$table.'`');
        $handle = $this->openCsvFile($file, $skip);

        for ($current_line = 0; $line = fgetcsv($handle, MAX_LINE_SIZE, $this->SEPARATOR); $current_line++) {

            if (empty($line)) {
                continue;
            }

            // fetch data
            $data = array();
            $i = 0;
            foreach ($header as $head) {
                $data[$head] = @$line[$i];
                $i++;
            }

            // insert data into db
            $this->db->insert($table, $data, false, false, Db::INSERT_IGNORE,false);

            $error = $this->db->getMsgError();
            if ($error) {
                PrestaShopLogger::addLog($error . " - (" . implode(', ', $line) . ")", 2);
            }
        }
        $this->closeCsvFile($handle);

        return true;
    }

    /**
     * @param string $file absolute path to file
     * @param int    $skip
     *
     * @return bool|false|resource
     */
    public function openCsvFile($file = 'file.csv', $skip = 0)
    {   
        $handle = false;
        if (is_file($file) && is_readable($file)) {
            $handle = fopen($file, 'r');
        }

        if (!$handle) {
            die('Cannot read the .CSV file');
        }

        $this->rewindBomAware($handle);

        // Skippa le prime n righe
        for ($i = 0; $i < (int)$skip; ++$i) {
            fgetcsv($handle, MAX_LINE_SIZE, $this->SEPARATOR);
        }
        return $handle;
    }

    /**
     * @param $handle
     */
    public function closeCsvFile($handle)
    {
        fclose($handle);
    }

    /**
     * @param $handle
     *
     * @return bool
     */
    protected function rewindBomAware($handle)
    {
        // A rewind wrapper that skips BOM signature wrongly
        if (!is_resource($handle)) {
            return false;
        }
        rewind($handle);
        if (($bom = fread($handle, 3)) != "\xEF\xBB\xBF") {
            rewind($handle);
        }

        return true;
    }
}