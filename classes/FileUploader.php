<?php

class FileUploader
{
    var $maxSize = null;
    var $fileFieldName = null;
    var $fileTypes = array("product", "customer", "omaggi", "conai");

    function __construct($max_size=100*1024*1024, $file_field_name="file"){
        $this->maxSize = $max_size;
        $this->fileFieldName = $file_field_name;
    }

    public function upload($file, $destination, $file_type){
        // File field name check
        if (array_key_exists($this->fileFieldName, $file)) {
            $file = $file[$this->fileFieldName];

            // Error check
            if ($file['error'] === UPLOAD_ERR_OK) {

                // Size check
                if ($file["size"] <= $this->maxSize) {

                    // File type check
                    if ($this->fileTypeExists($file_type)) {
                        if (!move_uploaded_file($file['tmp_name'], $destination . DIRECTORY_SEPARATOR . $file_type . ".csv")) {
                            $message = "Error on move_uploaded_file";
                        } else {
                            return "Success";
                        }
                    } else {
                        $message = "File type" . $file_type . "doesn't exist";
                    }
                } else {
                    $message = "File too large";
                }
            } else {
                $message = $this->codeToMessage($file['error']);
            }
        } else {
            $message = "File field name not found";
        }
        return $message;
    }

    private function codeToMessage($code)
    {
        switch ($code) {
            case UPLOAD_ERR_INI_SIZE:
                $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
                break;
            case UPLOAD_ERR_PARTIAL:
                $message = "The uploaded file was only partially uploaded";
                break;
            case UPLOAD_ERR_NO_FILE:
                $message = "No file was uploaded";
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $message = "Missing a temporary folder";
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $message = "Failed to write file to disk";
                break;
            case UPLOAD_ERR_EXTENSION:
                $message = "File upload stopped by extension";
                break;

            default:
                $message = "Unknown upload error";
                break;
        }
        return $message;
    }

    private function fileTypeExists($file_type){
        if (in_array($file_type, $this->fileTypes)){
            return true;
        }
        return false;
    }
}