<?php
include '../../../config/config.inc.php';
require_once  '../propacimport.php';
$propac_import = new Propacimport();

$csv_clients = _PS_MODULE_DIR_.'propacimport/csv/customer.csv';
$propac_import->importClients($csv_clients);
unlink(_PS_MODULE_DIR_.'propacimport/csv/customer.csv');