<?php
include '../../../config/config.inc.php';
require_once  '../propacimport.php';
$propac_import = new Propacimport();

$csv_giacenze = _PS_MODULE_DIR_.'propacimport/csv/product.csv';
$propac_import->importGiacenze($csv_giacenze);
unlink(_PS_MODULE_DIR_.'propacimport/csv/product.csv');