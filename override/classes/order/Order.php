<?php
/**
 * 2007-2018 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
use PrestaShop\PrestaShop\Adapter\ServiceLocator;

class Order extends OrderCore
{
    public $customer_cod_gest = '';

    public function __construct($id = null, $id_lang = null)
    {
        parent::$definition['fields'] = array_merge(parent::$definition['fields'],array('customer_cod_gest' => array('type' => self::TYPE_STRING),));
        parent::__construct($id, $id_lang);

        if ($this->id_customer){
            $customer = $this->getCustomer();
            if ($this->customer_cod_gest != $customer->getCodGest()){
                $this->customer_cod_gest = $customer->getCodGest(); //update codice gestionale if diferent
                $this->update();
            }
        }
    } //override method



}
