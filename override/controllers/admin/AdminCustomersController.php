<?php
/**
 * 2007-2018 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

/**
 * TODo aggiungere questo codice nella nuova classe CustomerController al interno del path src/PrestaShopBundle/Controller/Admin/Sell/Customer
 * @property Customer $object
 */
class AdminCustomersController extends AdminCustomersControllerCore
{
    public function __construct()
    {
        parent::__construct();

        $this->fields_list = array_merge($this->fields_list, array(
          'cod_gest' => array(
            'title' => $this->trans('Codice Gestionale', array(), 'Admin.Global'),
            'maxlength' => 50,
          ),
        ));
    }   //overide method

    public function renderForm()
    {

        if (!($obj = $this->loadObject(true))) {
            return;
        }
        $this->fields_form_override = array(
          array(
            'type' => 'text',
            'label' => $this->trans('Codice Gestionale', array(), 'Admin.Global'),
            'name' => 'cod_gest',
            'col' => '4',
            'required' => false,
            'autocomplete' => false,
          ),
        );
        return parent::renderForm();
    }   //overide method




}
