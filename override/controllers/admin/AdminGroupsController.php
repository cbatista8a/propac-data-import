<?php
/**
 * 2007-2018 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

/**
 * @property Group $object
 */
class AdminGroupsController extends AdminGroupsControllerCore
{
    public function __construct()
    {

        parent::__construct();

        $this->fields_list = array(
            'id_group' => array(
                'title' => $this->trans('ID', array(), 'Admin.Global'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'name' => array(
                'title' => $this->trans('Group name', array(), 'Admin.Shopparameters.Feature'),
                'filter_key' => 'b!name',
            ),
            'reduction' => array(
                'title' => $this->trans('Discount (%)', array(), 'Admin.Shopparameters.Feature'),
                'align' => 'right',
                'type' => 'percent',
            ),
            'nb' => array(
                'title' => $this->trans('Members', array(), 'Admin.Shopparameters.Feature'),
                'align' => 'center',
                'havingFilter' => true,
            ),
            'show_prices' => array(
                'title' => $this->trans('Show prices', array(), 'Admin.Shopparameters.Feature'),
                'align' => 'center',
                'type' => 'bool',
                'orderby' => false,
            ),
            'aliquota_iva' => array(
              'title' => $this->trans('IVA (%)', array(), 'Admin.Shopparameters.Feature'),
              'align' => 'right',
              'type' => 'percent',
            ),
            'date_add' => array(
                'title' => $this->trans('Creation date', array(), 'Admin.Shopparameters.Feature'),
                'type' => 'date',
                'align' => 'right',
            ),
        );

    } //override method

    public function renderForm()
    {
        if (!($group = $this->loadObject(true))) {
            return;
        }

        $this->fields_form = array(
          'legend' => array(
            'title' => $this->trans('Customer group', array(), 'Admin.Shopparameters.Feature'),
            'icon' => 'icon-group',
          ),
          'submit' => array(
            'title' => $this->trans('Save', array(), 'Admin.Actions'),
          ),
          'input' => array(
            array(
              'type' => 'text',
              'label' => $this->trans('Name', array(), 'Admin.Global'),
              'name' => 'name',
              'required' => true,
              'lang' => true,
              'col' => 4,
              'hint' => $this->trans('Forbidden characters:', array(), 'Admin.Notifications.Info') . ' 0-9!&amp;lt;&amp;gt;,;?=+()@#"�{}_$%:',
            ),
            array(
              'type' => 'text',
              'label' => $this->trans('Discount', array(), 'Admin.Global'),
              'name' => 'reduction',
              'suffix' => '%',
              'col' => 1,
              'hint' => $this->trans('Automatically apply this value as a discount on all products for members of this customer group.', array(), 'Admin.Shopparameters.Help'),
            ),
            array(
              'type' => 'text',
              'label' => $this->trans('IVA', array(), 'Admin.Global'),
              'name' => 'aliquota_iva',
              'suffix' => '%',
              'col' => 1,
              'hint' => $this->trans('Apply this value as a IVA on all products for members of this customer group.', array(), 'Admin.Shopparameters.Help'),
            ),
            array(
              'type' => 'select',
              'label' => $this->trans('Price display method', array(), 'Admin.Shopparameters.Feature'),
              'name' => 'price_display_method',
              'col' => 2,
              'hint' => $this->trans('How prices are displayed in the order summary for this customer group.', array(), 'Admin.Shopparameters.Help'),
              'options' => array(
                'query' => array(
                  array(
                    'id_method' => PS_TAX_EXC,
                    'name' => $this->trans('Tax excluded', array(), 'Admin.Global'),
                  ),
                  array(
                    'id_method' => PS_TAX_INC,
                    'name' => $this->trans('Tax included', array(), 'Admin.Global'),
                  ),
                ),
                'id' => 'id_method',
                'name' => 'name',
              ),
            ),
            array(
              'type' => 'switch',
              'label' => $this->trans('Show prices', array(), 'Admin.Shopparameters.Feature'),
              'name' => 'show_prices',
              'required' => false,
              'class' => 't',
              'is_bool' => true,
              'values' => array(
                array(
                  'id' => 'show_prices_on',
                  'value' => 1,
                  'label' => $this->trans('Enabled', array(), 'Admin.Global'),
                ),
                array(
                  'id' => 'show_prices_off',
                  'value' => 0,
                  'label' => $this->trans('Disabled', array(), 'Admin.Global'),
                ),
              ),
              'hint' => $this->trans('Customers in this group can view prices.', array(), 'Admin.Shopparameters.Help'),
            ),
            array(
              'type' => 'group_discount_category',
              'label' => $this->trans('Category discount', array(), 'Admin.Shopparameters.Feature'),
              'name' => 'reduction',
              'values' => ($group->id ? $this->formatCategoryDiscountList((int) $group->id) : array()),
            ),
            array(
              'type' => 'modules',
              'label' => $this->trans('Modules authorization', array(), 'Admin.Shopparameters.Feature'),
              'name' => 'auth_modules',
              'values' => $this->formatModuleListAuth($group->id),
            ),
          ),
        );
        $this->fields_value['aliquota_iva'] = isset($group->aliquota_iva) ? $group->aliquota_iva : 0;
        if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = array(
              'type' => 'shop',
              'label' => $this->trans('Shop association', array(), 'Admin.Global'),
              'name' => 'checkBoxShopAsso',
            );
        }

        if (Tools::getIsset('addgroup')) {
            $this->fields_value['price_display_method'] = Configuration::get('PRICE_DISPLAY_METHOD');
        }
        if (!$this->multiple_fieldsets) {
            $this->fields_form = array(array('form' => $this->fields_form));
        }

        // For add a fields via an override of $fields_form, use $fields_form_override
        if (is_array($this->fields_form_override) && !empty($this->fields_form_override)) {
            $this->fields_form[0]['form']['input'] = array_merge($this->fields_form[0]['form']['input'], $this->fields_form_override);
        }
        $this->fields_value['reduction'] = isset($group->reduction) ? $group->reduction : 0;

        $tree = new HelperTreeCategories('categories-tree');
        $this->tpl_form_vars['categoryTreeView'] = $tree->setRootCategory((int) Category::getRootCategory()->id)->render();

        $fields_value = $this->getFieldsValue($this->object);
        Hook::exec('action' . $this->controller_name . 'FormModifier', array(
          'object' => &$this->object,
          'fields' => &$this->fields_form,
          'fields_value' => &$fields_value,
          'form_vars' => &$this->tpl_form_vars,
        ));
        $helper = new HelperForm($this);
        $this->setHelperDisplay($helper);
        $helper->fields_value = $fields_value;
        $helper->submit_action = $this->submit_action;
        $helper->tpl_vars = $this->getTemplateFormVars();
        $helper->show_cancel_button = (isset($this->show_form_cancel_button)) ? $this->show_form_cancel_button : ($this->display == 'add' || $this->display == 'edit');

        $back = urldecode(Tools::getValue('back', ''));
        if (empty($back)) {
            $back = self::$currentIndex . '&token=' . $this->token;
        }
        if (!Validate::isCleanHtml($back)) {
            die(Tools::displayError());
        }

        $helper->back_url = $back;
        !is_null($this->base_tpl_form) ? $helper->base_tpl = $this->base_tpl_form : '';
        if ($this->access('view')) {
            if (Tools::getValue('back')) {
                $helper->tpl_vars['back'] = Tools::safeOutput(Tools::getValue('back'));
            } else {
                $helper->tpl_vars['back'] = Tools::safeOutput(Tools::getValue(self::$currentIndex . '&token=' . $this->token));
            }
        }
        $form = $helper->generateForm($this->fields_form);

        return $form;
    } //ovveride method

}
