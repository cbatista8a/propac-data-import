<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * In some cases you should not drop the tables.
 * Maybe the merchant will just try to reset the module
 * but does not want to loose all of the data associated to the module.
 */
/******* Uncomment for delete all custom columns that was previous installed *******/
/*// format table => column
$columns['customer'] = 'cod_gest';
$columns['orders'] = 'customer_cod_gest';
$columns['group'] = 'aliquota_iva';
$columns['stock_available'] = 'propac_quantity_code';
$columns['product_attribute'] = 'conai';
$columns['product_attribute'] = 'pallet';
$columns['product_attribute'] = 'sconto_pallet';
$columns['product_attribute'] = 'quantity_per_pack';

foreach ($columns as $table => $column) {
    $result = Db::getInstance()->executeS('SELECT * FROM information_schema.columns WHERE table_name = "'._DB_PREFIX_.$table.'" and column_name = "'.$column.'" ;');
    if (is_array($result) && count($result)>0){
        if (Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.$table.' DROP COLUMN '.$column) == false) {
            return false;
        }
    }
}*/

//delete tables
$sql = array();

$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'propacimport_customer`;';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'propacimport_giacenze`;';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'conai_values`;';

foreach ($sql as $query){
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}