<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'propacimport_customer` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `codice_cliente` varchar(20),
    `dni` varchar(50),
    `ragione_sociale` varchar(100),
    `date_upd` datetime,
    `sconto_conai` float(11),
    `aliquota_iva` int(11),
    PRIMARY KEY  (`id`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'propacimport_giacenze` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `codice_prodotto` varchar(50),
    `disponibilita` int(11) DEFAULT 0,
    `codice_combinazione` varchar(50),
    `pallet` int(11) DEFAULT 0,
    `unita_vendita` int(11) DEFAULT 1,
    PRIMARY KEY  (`id`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'conai_values` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `codice_prodotto` varchar(50),
    `nome_prodotto` varchar(200),
    `codice_materiale` varchar(50),
    `importo_per_kg` varchar(50),
    `peso_kg` varchar(50),
    `importo_conai` varchar(50),
    PRIMARY KEY  (`id`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';


foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}

$sql_alter[] = 'ALTER TABLE `' . _DB_PREFIX_ . 'customer` ADD COLUMN `cod_gest` varchar(50) AFTER `email`;';
$sql_alter[] = 'ALTER TABLE `' . _DB_PREFIX_ . 'customer` ADD COLUMN `dni` varchar(16) AFTER `siret`;';
$sql_alter[] = 'ALTER TABLE `' . _DB_PREFIX_ . 'customer` ADD COLUMN `split_payment` tinyint(1) AFTER `dni`;';
$sql_alter[] = 'ALTER TABLE `' . _DB_PREFIX_ . 'orders` ADD COLUMN `customer_cod_gest` varchar(50) AFTER `reference`;';
$sql_alter[] = 'ALTER TABLE `' . _DB_PREFIX_ . 'group` ADD COLUMN `aliquota_iva` int(11) AFTER `show_prices`;';
$sql_alter[] = 'ALTER TABLE `' . _DB_PREFIX_ . 'stock_available` ADD COLUMN `propac_quantity_code` int(11) ;';
$sql_alter[] = 'ALTER TABLE `' . _DB_PREFIX_ . 'product_attribute` ADD COLUMN `conai` decimal(11,10) ;';
$sql_alter[] = 'ALTER TABLE `' . _DB_PREFIX_ . 'product_attribute` ADD COLUMN `quantity_per_pack` decimal(11) DEFAULT 1;';
$sql_alter[] = 'ALTER TABLE `' . _DB_PREFIX_ . 'product_attribute` ADD COLUMN `pallet` decimal(11) DEFAULT 0;';
$sql_alter[] = 'ALTER TABLE `' . _DB_PREFIX_ . 'product_attribute` ADD COLUMN `sconto_pallet` decimal(11,2) DEFAULT 0;';
$sql_alter[] = 'CREATE INDEX dni ON `' . _DB_PREFIX_ . 'propacimport_customer` (dni);';

foreach ($sql_alter as $query) {
    if (Db::getInstance()->execute($query) == false) {
        continue; //if column exist return false but is non an error, because that, continue
    }
}
